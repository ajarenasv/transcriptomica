# Trabajo de Trancriptómica del Master de Bioinformática 2021/2022

## Estructura del repositorio

En este repositorio se encuentran almacenados todos los ficheros generados durante el análisis de RNA-seq pedido en el enunciado.

La estructura del repositorio y los archivos que contiene es la siguiente:

- README.md: Este archivo
- Enunciado.pdf: Enunciado del trabajo.
- envs/rnaseq.yml: Entorno conda necesario para ejecutar correctamente los scripts de ambos apartados.
- Apartado1/: Contiene los archivos asociados a la resolución del Apartado1. Ver el archivo [README.md](Apartado1/README.md) de este directorio para más información sobre su contenido.
- Apartado2/: Contiene los archivos asociados a la resolución del Apartado2. Ver el archivo [README.md](Apartado2/README.md) de este directorio para más información sobre su contenido.

## Configuración del espacio de Trabajo

### Descarga del repositorio

La primera tarea a realizar es clonar el repositorio para poder ejecutar los scripts que contiene. Para ello, ejecutaremos desde el directorio de trabajo que queramos la siguiente instrucción:

```console
~$ git clone https://gitlab.com/ajarenasv/transcriptomica.git
```

### Creación y activación del entorno *conda*

A continuación procederemos a crear el entorno conda incluido, y a su activación:

```console
~$ cd transcriptomica/envs
~/transcriptomica/envs$ conda env create -f rnaseq.yml
~/transcriptomica/envs$ conda activate rnaseq
(rnaseq) ~/transcriptomica/envs$ cd ..
(rnaseq) ~/transcriptomica$
```

## Resolución de los apartados del ejercicio

Ya tenemos el entorno preparado para ejecutar los scripts pertenecientes a ambos apartados. Para continuar con la resolución del ejercicio se puede acceder a los siguientes ficheros:

### Apartado 1

- [Apartado1/README.md](Apartado1/README.md): Contiene las instrucciones necesarias para generar los ficheros de output del análisis RNA-seq, así como las respuestas a las preguntas planteadas en el enunciado y la interpretación de los resultados obtenidos.

### Apartado 2

- [Apartado2/README.md](Apartado2/README.md): Contiene las instrucciones necesarias para ejecutar el análisis de expresión diferencial, así como las respuestas a las preguntas planteadas en el enunciado y la interpretación de los resultados obtenidos.

## Versiones de software usadas

El listado completo del software usado, así como sus dependencias puede encontrarse en el entorno conda incluido en el repositorio. No obstante, se incluye el listado de los principales programas y librerías utilizados:

- Apartado 1:
  * fastqc 0.11.9
  * hisat2 2.2.1
  * htseq 2.0.1
  * multiqc 1.12
  * samtools 1.15.1
  * r-base 4.1.3
  * bioconductor-deseq2 1.34.0


- Apartado 2:
  * r-base 4.1.3
  * bioconductor-deseq2 1.34.0
  * bioconductor-apeglm 1.16.0
  * bioconductor-deseq2 1.34.0
  * bioconductor-enhancedvolcano 1.12.0
  * bioconductor-fgsea 1.20.0
  * bioconductor-org.hs.eg.db 3.14.0
  * r-data.table 1.14.2
  * r-dplyr 1.0.8
  * r-ggplot2 3.3.5
  * r-pheatmap 1.0.12
  * r-rcolorbrewer 1.1_3
  * RStudio 2021.09.0+351 "Ghost Orchid" Release 
