#!/bin/bash

# Define some directories
DATADIR=input
REFDIR=input/REF
LOGDIR=log
HISATOUT=hisat2

HISATSEED=123
HISATTHREADS=6

indexNumFiles=8

printlog() {
    echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` $0: $1"
}

if ! [ -x "$(command -v hisat2)" ]; then
  printlog 'Error: hisat2 is not installed.'
  exit 1
fi

if ! [ -x "$(command -v samtools)" ]; then
  printlog 'Error: samtools is not installed.'
  exit 1
fi

mkdir -p $LOGDIR

files=$(ls $REFDIR/*fa 2>/dev/null)
for file in $files
do
    out_indexbase=$(echo $file | rev | cut -f 2- -d '.' | rev)

    # Check if the directory contains index files (if the genome is already indexed, it should be $indexNumFiles files).
    numfiles=$(ls -1 ${out_indexbase}*ht2 2>/dev/null | wc -l)
    if [ "$numfiles" -eq "$indexNumFiles" ]; then
        printlog "Directory ${out_indexbase} already contains $numfiles files. Skipping indexing the genome file."
    else
	printlog "Indexing reference genome file '$file' with index base '$out_indexbase'"
        hisat2-build --seed $HISATSEED -p $HISATTHREADS $file $out_indexbase >> $LOGDIR/hisat2-build.log 2>&1
	if [ "$?" -ne 0 ]; then
	    printlog "Error executing 'hisat2-build'"
	    exit 1
	fi
        printlog "Indexing of file '$file' done. Check output files on dir '$REFDIR' and log on '$LOGDIR'/hisat2-build.log"
    fi
done

mkdir -p $HISATOUT

sampleIDs=$(ls $DATADIR/*fastq 2>/dev/null | cut -d. -f1 | sort | uniq | sed "s:${DATADIR}/::")
for sid in $sampleIDs
do
    if [ -f $HISATOUT/${sid}.sam ]; then
        printlog "File '$HISATOUT/${sid}.sam' exists. Skipping alignment."
    else
        indexbase=$(ls $REFDIR/*1.ht2 2> /dev/null | cut -f 1 -d.)
        printlog "Aligning sample '$sid'"
        hisat2 --new-summary --summary-file $HISATOUT/${sid}.hisat2.summary --rna-strandness R --seed 123 --phred33 -p 6 -k 1 -x input/REF/chr21 -U $DATADIR/${sid}.chr21.fastq -S $HISATOUT/${sid}.sam 2> $LOGDIR/hisat2.${sid}.log
        if [ "$?" -ne 0 ]; then
            printlog "Error executing 'hisat2'"
            exit 1
        fi
        grep "Overall alignment rate" $LOGDIR/hisat2.${sid}.log
    fi
done

printlog "Alignment done. Check output files on dir '$HISATOUT' and log on '$LOGDIR'/hisat2*.log"

for sid in $sampleIDs
do
    if [ -f $HISATOUT/${sid}.bam ]; then
        printlog "File '$HISATOUT/${sid}.bam' exists. Skipping conversion."
    else
        printlog "Converting, sorting and indexing $HISATOUT/${sid}.sam"

	# Conversion to BAM
        samtools view -bh $HISATOUT/${sid}.sam > $HISATOUT/${sid}.bam 2> $LOGDIR/samtools.${sid}.log
        if [ "$?" -ne 0 ]; then
            printlog "Error executing 'samtools'"
            exit 1
        fi

	# Sorting BAM
        samtools sort $HISATOUT/${sid}.bam -o $HISATOUT/${sid}.sorted.bam >> $LOGDIR/samtools.${sid}.log 2>&1
        if [ "$?" -ne 0 ]; then
            printlog "Error executing 'samtools'"
            exit 1
        fi

	# Indexing BAM
        samtools index $HISATOUT/${sid}.sorted.bam >> $LOGDIR/samtools.${sid}.log 2>&1
        if [ "$?" -ne 0 ]; then
            printlog "Error executing 'samtools'"
            exit 1
        fi
    fi
done

printlog "Conversion to BAM done. Check output files on dir '$HISATOUT' and log on '$LOGDIR'/samtools*.log"

