#!/bin/bash

# Define some directories
DATADIR=input
FASTQCOUT=fastQC
LOGDIR=log

printlog() {
    echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` $0: $1"
}

if ! [ -x "$(command -v fastqc)" ]; then
  printlog 'Error: fastqc is not installed.'
  exit 1
fi

mkdir -p $FASTQCOUT
mkdir -p $LOGDIR

files=$(ls $DATADIR/*fastq 2>/dev/null)
for file in $files
do
    sampleID=$(echo $file | cut -d. -f 1 | sed "s:input/::")
    if [ -f $FASTQCOUT/${sampleID}*.html ]; then
        printlog "File '$FASTQCOUT/${sampleID}*.html' exists. Skipping quality check."
    else
        printlog "Performing quality check on '$sampleID' (file $file)"
        fastqc -o $FASTQCOUT --noextract $file >> $LOGDIR/fastqc.${sampleID}.log 2>&1
        if [ "$?" -ne 0 ]; then
            printlog "Error executing 'fastqc'"
            exit 1
        fi
    fi
done

printlog "Quality checks done. Check output on dir '$FASTQCOUT' and logs on '$LOGDIR'"

