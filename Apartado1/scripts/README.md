# Scripts Apartado 1: Análisis RNA-seq

---
> **_IMPORTANTE:_**  Los siguientes scripts deben ejecutarse desde el directorio "Apartado1" con el ambiente conda "rnaseq" previamente activado.
---

## qc.sh

Uso: `qc.sh`

Script para ejecutar un análisis de calidad sobre los archivos fastq mediante la herramienta fastqc (pregunta 1).


## align.sh

Uso: `align.sh`

Script que realiza la indexación de la referencia genómica y el alineamiento de las muestras (pregunta 2)-

## count.sh

Uso: `count.sh`

Script para realizar el contaje de lecturas y generación de ficheros asociados (pregunta 3). Usa el script R *DESeq2_create_counts.R*.

## pipeline.sh

Uso: `pipeline.sh`

Pipeline script para efectuar análisis RNA-seq.

Este pipeline efectúa las siguientes llamadas al resto de scripts:
- *qc.sh* para ejecutar un análisis de calidad.
- *align.sh* para indexar el genoma de referencia y alinear los archivos fastq.
- *count.sh* para generar los ficheros de cuentas necesarias para un análisis de expresión. Este script llama a su vez al script R *DESeq2_create_counts.R*.
- Por último, el script usa *multiqc* para generar un informe sobre el resultado de las anteriores operaciones.

## clean.sh

Uso: `clean.sh`

Elimina los directorios y ficheros temporales y de resultados. Usado para hacer pruebas.

> ¡¡ Usar con precaución !!
