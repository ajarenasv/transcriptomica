#!/bin/bash

SCRIPTDIR=scripts
OUTDIR=output
LOGDIR=log

# Store start time
startTime=$(date +%s)

printlog() {
    echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` $0: $1"
}

printlog "############################################################"
printlog "#"
printlog "# PIPELINE STARTED"
printlog "#"
printlog "############################################################"

bash $SCRIPTDIR/qc.sh
if [ "$?" -ne 0 ]; then
    printerror "Error executing 'qc.sh'"
    exit 1
fi

bash $SCRIPTDIR/align.sh
if [ "$?" -ne 0 ]; then
    printerror "Error executing 'align.sh'"
    exit 1
fi

bash $SCRIPTDIR/count.sh
if [ "$?" -ne 0 ]; then
    printerror "Error executing 'count.sh'"
    exit 1
fi

if ! [ -x "$(command -v multiqc)" ]; then
  printlog 'Error: multiqc is not installed.'
  exit 1
fi

if [ -f $OUTDIR/multiqc_report.html ]; then
    printlog "MultiQC report already exists. Skipping."
else
    multiqc -o $OUTDIR . >> $LOGDIR/multiqc.log 2>&1
    if [ "$?" -ne 0 ]; then
        printlog "Error executing 'MultiQC'"
        exit 1
    fi
    printlog "MultiQC report created. Check output files on dir '$OUTDIR' and log on '$LOGDIR/multiqc.log'"
fi

# Store finish time
finishTime=$(date +%s)

printlog ""
printlog "############################################################"
printlog "#"
printlog "# PIPELINE FINISHED: Check $OUTDIR and $LOGDIR directories for"
printlog "# more information."
printlog "#"
printlog "# Time elapsed: $(( (finishTime - startTime) )) seconds"
printlog "#"
printlog "############################################################"

