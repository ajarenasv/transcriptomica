#!/bin/bash

# Define some directories
REFFILE=input/REF/GRCh38.gencode.v38.annotation.for.chr21.gtf
LOGDIR=log
SCRIPTDIR=scripts
OUTDIR=output
HISATOUT=hisat2
HTSEQOUT=htseq

printlog() {
    echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` $0: $1"
}

if ! [ -x "$(command -v htseq-count)" ]; then
  printlog 'Error: htseq is not installed.'
  exit 1
fi

mkdir -p $LOGDIR
mkdir -p $HTSEQOUT

sampleIDs=$(ls $HISATOUT/*sorted.bam 2>/dev/null | cut -d. -f1 | sort | uniq | sed "s:${HISATOUT}/::")
for sid in $sampleIDs
do
    if [ -f $HTSEQOUT/${sid}.htseq ]; then
        printlog "File '$HTSEQOUT/${sid}.htseq' exists. Skipping counting."
    else
        printlog "Counting reads for sample '$sid'"
        htseq-count --format=bam --stranded=reverse --mode=intersection-nonempty --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name $HISATOUT/${sid}.sorted.bam $REFFILE > $HTSEQOUT/${sid}.htseq 2> $LOGDIR/htseq.${sid}.log
        if [ "$?" -ne 0 ]; then
            printlog "Error executing 'htseq-count'"
            exit 1
        fi
    fi
done

printlog "Counting reads done. Check output files on dir '$HTSEQOUT' and log on '$LOGDIR/htseq*.log'"

if ! [ -x "$(command -v Rscript)" ]; then
  printlog 'Error: R is not installed.'
  exit 1
fi

mkdir -p $OUTDIR

if [ -f $OUTDIR/rawcounts.tsv -a -f $OUTDIR/normalizedcounts.tsv ]; then
    printlog "Raw and normalized counts files exists. Skipping creation."
else
    printlog "Creating raw and normalized counts files in R."
    Rscript $SCRIPTDIR/DESeq2_create_counts.R >> $LOGDIR/r.log 2>&1
    if [ "$?" -ne 0 ]; then
        printlog "Error executing 'Rscript'"
        exit 1
    fi
    printlog "Counts files created. Check output files on dir '$OUTDIR' and log on '$LOGDIR/r.log'"
fi


