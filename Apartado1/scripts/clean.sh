#!/bin/sh

rm -rf log
rm -rf fastQC
rm -rf input/REF/*ht2
rm -rf hisat2
rm -rf htseq
rm -rf output
rm -rf multiqc_data

