# Trabajo de Trancriptómica del Master de Bioinformática 2021/2022

## Apartado1

Este directorio contiene los scripts y ficheros de input necesarios para realizar el análisis RNA-seq solicitado en el enunciado. La estructura de archivos es la siguiente:

* README.md (este archivo): Contiene las instrucciones para ejecutar el análisis RNA-seq con los scripts proporcionados, las respuestas a las preguntas planteadas en el enunciado y la interpretación de los resultados obtenidos.
* images: Contiene los gráficos a analizar.
* input: Archivos de entrada.
    + SRR348037[1234].fastq: Archivos fastq correspondientes a 4 muestras diferentes.
    + REF/chr21.fa: Secuencia de la referencia genómica correspondiente al cromosoma 21 humano (ensamblaje GRCh38).
    + REF/GRCh38.gencode.v38.annotation.for.chr21.gtf: Fichero con la anotación génica para los genes del cromosoma 21.
* scripts: Scripts bash/R que ejecutan el pipeline usado para la análisis requerido:
    + README.md: Documentación sobre el uso de los scripts.
    + align.sh: Script que realiza la indexación de la referencia genómica y el alineamiento de las muestras (pregunta 2).
    + clean.sh: Script auxiliar para borrar ficheros de salida e intermedios.
    + count.sh: Script para realizar el contaje de lecturas y generación de ficheros asociados (pregunta 3).
    + DESeq2_create_counts.R: Script en R auxliar para el script *count.sh*.
    + pipeline.sh: Script que ejecuta el análisis completo llamando al resto de scripts de manera secuencial.
    + qc.sh: Script para ejecutar un análisis de calidad sobre los archivos fastq mediante la herramienta fastqc (pregunta 1).

---
### Ejecución del Pipeline

> NOTA: Es necesario tener activo el ambiente *rnaseq* proporcionado en el repositorio para la correcta ejecución de los scripts. En caso de que un script no encuntre un programa necesario, mostrará un mensaje de error y terminará la ejecución.

Se recomienda ejecutar el script *pipeline.sh* desde el directorio *Apartado1* para ejecutar todos los procesos necesarios para el análisis RNA-seq, aunque también se pueden ir ejecutando los scripts por separado.

> NOTA: Si se ejecutan los scripts por separado habrá que ejecutar manualmente el programa *multiqc*, ya que éste se ejecuta únicamente desde el script *pipeline.sh*.

Al ejecutar el script *pipeline.sh* se obtendrá una salida similar a la siguiente, donde se pueden ver los pasos ejecutados por cada script:

<details>
<summary>Expandir para ver la salida del comando *pipeline.sh*</summary>

```
(rnaseq) ~/transcriptomica/Apartado1$ scripts/pipeline.sh
[2022/04/16 18:52:57] scripts/pipeline.sh: ############################################################
[2022/04/16 18:52:57] scripts/pipeline.sh: #
[2022/04/16 18:52:57] scripts/pipeline.sh: # PIPELINE STARTED
[2022/04/16 18:52:57] scripts/pipeline.sh: #
[2022/04/16 18:52:57] scripts/pipeline.sh: ############################################################
[2022/04/16 18:52:57] scripts/qc.sh: Performing quality check on 'SRR3480371' (file input/SRR3480371.chr21.fastq)
[2022/04/16 18:53:03] scripts/qc.sh: Performing quality check on 'SRR3480372' (file input/SRR3480372.chr21.fastq)
[2022/04/16 18:53:08] scripts/qc.sh: Performing quality check on 'SRR3480373' (file input/SRR3480373.chr21.fastq)
[2022/04/16 18:53:13] scripts/qc.sh: Performing quality check on 'SRR3480374' (file input/SRR3480374.chr21.fastq)
[2022/04/16 18:53:19] scripts/qc.sh: Quality checks done. Check output on dir 'fastQC' and logs on 'log'
[2022/04/16 18:53:19] scripts/align.sh: Indexing reference genome file 'input/REF/chr21.fa' with index base 'input/REF/chr21'
[2022/04/16 18:53:41] scripts/align.sh: Indexing of file 'input/REF/chr21.fa' done. Check output files on dir 'input/REF' and log on 'log'/hisat2-build.log
[2022/04/16 18:53:41] scripts/align.sh: Aligning sample 'SRR3480371'
	Overall alignment rate: 99.76%
[2022/04/16 18:53:43] scripts/align.sh: Aligning sample 'SRR3480372'
	Overall alignment rate: 99.79%
[2022/04/16 18:53:46] scripts/align.sh: Aligning sample 'SRR3480373'
	Overall alignment rate: 99.85%
[2022/04/16 18:53:51] scripts/align.sh: Aligning sample 'SRR3480374'
	Overall alignment rate: 99.77%
[2022/04/16 18:53:55] scripts/align.sh: Alignment done. Check output files on dir 'hisat2' and log on 'log'/hisat2*.log
[2022/04/16 18:53:55] scripts/align.sh: Converting, sorting and indexing hisat2/SRR3480371.sam
[2022/04/16 18:53:58] scripts/align.sh: Converting, sorting and indexing hisat2/SRR3480372.sam
[2022/04/16 18:54:02] scripts/align.sh: Converting, sorting and indexing hisat2/SRR3480373.sam
[2022/04/16 18:54:09] scripts/align.sh: Converting, sorting and indexing hisat2/SRR3480374.sam
[2022/04/16 18:54:13] scripts/align.sh: Conversion to BAM done. Check output files on dir 'hisat2' and log on 'log'/samtools*.log
[2022/04/16 18:54:13] scripts/count.sh: Counting reads for sample 'SRR3480371'
[2022/04/16 18:54:32] scripts/count.sh: Counting reads for sample 'SRR3480372'
[2022/04/16 18:54:58] scripts/count.sh: Counting reads for sample 'SRR3480373'
[2022/04/16 18:55:34] scripts/count.sh: Counting reads for sample 'SRR3480374'
[2022/04/16 18:56:03] scripts/count.sh: Counting reads done. Check output files on dir 'htseq' and log on 'log/htseq*.log'
[2022/04/16 18:56:03] scripts/count.sh: Creating raw and normalized counts files in R.
[2022/04/16 18:56:12] scripts/count.sh: Counts files created. Check output files on dir 'output' and log on 'log/r.log'
[2022/04/16 18:56:15] scripts/pipeline.sh: MultiQC report created. Check output files on dir 'output' and log on 'log/multiqc.log'
[2022/04/16 18:56:15] scripts/pipeline.sh:
[2022/04/16 18:56:15] scripts/pipeline.sh: ############################################################
[2022/04/16 18:56:15] scripts/pipeline.sh: #
[2022/04/16 18:56:15] scripts/pipeline.sh: # PIPELINE FINISHED: Check output and log directories for
[2022/04/16 18:56:15] scripts/pipeline.sh: # more information.
[2022/04/16 18:56:15] scripts/pipeline.sh: #
[2022/04/16 18:56:15] scripts/pipeline.sh: # Time elapsed: 198 seconds
[2022/04/16 18:56:15] scripts/pipeline.sh: #
[2022/04/16 18:56:15] scripts/pipeline.sh: ############################################################
```
</details>


En caso de querer repetir el análisis se puede invocar al script *clean.sh* que borará todos los ficheros auxiliares y de salida generados en el paso anterior.

```console
(rnaseq) ~/transcriptomica/Apartado1$ scripts/clean.sh
```

En caso de que algún fichero intermedio o final ya exista, los scripts lo detectarán y se saltarán el paso en cuestión. Si una vez ejecutado el pipeline, volvemos a lanzarlo, tendríamos la siguiente respuesta:

<details>
<summary>Expandir para ver la salida del comando *pipeline.sh*</summary>

```
(rnaseq) ~/transcriptomica/Apartado1$ scripts/pipeline.sh
[2022/04/16 19:05:29] scripts/pipeline.sh: ############################################################
[2022/04/16 19:05:29] scripts/pipeline.sh: #
[2022/04/16 19:05:29] scripts/pipeline.sh: # PIPELINE STARTED
[2022/04/16 19:05:29] scripts/pipeline.sh: #
[2022/04/16 19:05:29] scripts/pipeline.sh: ############################################################
[2022/04/16 19:05:29] scripts/qc.sh: File 'fastQC/SRR3480371*.html' exists. Skipping quality check.
[2022/04/16 19:05:29] scripts/qc.sh: File 'fastQC/SRR3480372*.html' exists. Skipping quality check.
[2022/04/16 19:05:29] scripts/qc.sh: File 'fastQC/SRR3480373*.html' exists. Skipping quality check.
[2022/04/16 19:05:29] scripts/qc.sh: File 'fastQC/SRR3480374*.html' exists. Skipping quality check.
[2022/04/16 19:05:29] scripts/qc.sh: Quality checks done. Check output on dir 'fastQC' and logs on 'log'
[2022/04/16 19:05:29] scripts/align.sh: Directory input/REF/chr21 already contains 8 files. Skipping indexing the genome file.
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480371.sam' exists. Skipping alignment.
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480372.sam' exists. Skipping alignment.
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480373.sam' exists. Skipping alignment.
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480374.sam' exists. Skipping alignment.
[2022/04/16 19:05:29] scripts/align.sh: Alignment done. Check output files on dir 'hisat2' and log on 'log'/hisat2*.log
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480371.bam' exists. Skipping conversion.
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480372.bam' exists. Skipping conversion.
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480373.bam' exists. Skipping conversion.
[2022/04/16 19:05:29] scripts/align.sh: File 'hisat2/SRR3480374.bam' exists. Skipping conversion.
[2022/04/16 19:05:29] scripts/align.sh: Conversion to BAM done. Check output files on dir 'hisat2' and log on 'log'/samtools*.log
[2022/04/16 19:05:29] scripts/count.sh: File 'htseq/SRR3480371.htseq' exists. Skipping counting.
[2022/04/16 19:05:29] scripts/count.sh: File 'htseq/SRR3480372.htseq' exists. Skipping counting.
[2022/04/16 19:05:29] scripts/count.sh: File 'htseq/SRR3480373.htseq' exists. Skipping counting.
[2022/04/16 19:05:29] scripts/count.sh: File 'htseq/SRR3480374.htseq' exists. Skipping counting.
[2022/04/16 19:05:29] scripts/count.sh: Counting reads done. Check output files on dir 'htseq' and log on 'log/htseq*.log'
[2022/04/16 19:05:29] scripts/count.sh: Raw and normalized counts files exists. Skipping creation.
[2022/04/16 19:05:29] scripts/pipeline.sh: MultiQC report already exists. Skipping.
[2022/04/16 19:05:29] scripts/pipeline.sh:
[2022/04/16 19:05:29] scripts/pipeline.sh: ############################################################
[2022/04/16 19:05:29] scripts/pipeline.sh: #
[2022/04/16 19:05:29] scripts/pipeline.sh: # PIPELINE FINISHED: Check output and log directories for
[2022/04/16 19:05:29] scripts/pipeline.sh: # more information.
[2022/04/16 19:05:29] scripts/pipeline.sh: #
[2022/04/16 19:05:29] scripts/pipeline.sh: # Time elapsed: 0 seconds
[2022/04/16 19:05:29] scripts/pipeline.sh: #
[2022/04/16 19:05:29] scripts/pipeline.sh: ############################################################
```
</details>

---
### Pregunta 1

Para ejecutar los pasos únicamente necesarios para ejecutar un control de calidad, podemos ejecutar la siguiente instrucción:

```console
(rnaseq) ~/transcriptomica/Apartado1$ scripts/qc.sh
```

Este script crea los directorios auxiliares y output necesarios y ejecuta el programa *fastqc* sobre cada archivo input de muestra.

Al terminar, se habrá generado en el directorio *Apartado1* una estructura de archivos similar a la siguiente:

```console
(rnaseq) ~/transcriptomica/Apartado1$ tree
.
├── [3.6M]  fastQC
│   ├── [575K]  SRR3480371.chr21_fastqc.html
│   ├── [334K]  SRR3480371.chr21_fastqc.zip
│   ├── [574K]  SRR3480372.chr21_fastqc.html
│   ├── [333K]  SRR3480372.chr21_fastqc.zip
│   ├── [583K]  SRR3480373.chr21_fastqc.html
│   ├── [346K]  SRR3480373.chr21_fastqc.zip
│   ├── [575K]  SRR3480374.chr21_fastqc.html
│   └── [331K]  SRR3480374.chr21_fastqc.zip
├── [ 36K]  log
│   ├── [ 980]  fastqc.SRR3480371.log
│   ├── [ 980]  fastqc.SRR3480372.log
│   ├── [ 980]  fastqc.SRR3480373.log
│   ├── [ 980]  fastqc.SRR3480374.log
```

El programa *fastqc* es invocado de la siguiente manera:

```console
fastqc -o $FASTQCOUT --noextract $file >> $LOGDIR/fastqc.${sampleID}.log 2>&1
```
Donde:
* *FASTQCOUT*: Indica el directorio de salida.
* *noextract*: Indica que el resultado se almacene en fichero zip, sin descomprimir.
* *file*: Es el nombre del fichero fastq a procesar.

Los logs de la ejecución se guardarán en un archivo fastqc.log por cada muestra.

A continuación se muestran los gráficos "Per base sequence quality" de cada una de las muestras:

<details>
<summary>SRR3480371 Per base sequence quality</summary>

![SRR3480371 Per base sequence quality](images/SRR3480371_pbsc.png)
</details>

<details>
<summary>SRR3480372 Per base sequence quality</summary>

![SRR3480372 Per base sequence quality](images/SRR3480372_pbsc.png)
</details>

<details>
<summary>SRR3480373 Per base sequence quality</summary>

![SRR3480373 Per base sequence quality](images/SRR3480373_pbsc.png)
</details>

<details>
<summary>SRR3480374 Per base sequence quality</summary>

![SRR3480374 Per base sequence quality](images/SRR3480374_pbsc.png)
</details>

Como se puede apreciar, las 4 gráficas son muy similares. En la parte superior se observa un boxplot que indica la calidad de la lectura (según el fastq) para esa posición en concreto. Se observa que las primeras bases de cada lectura tienen una calidad algo menor que el resto de bases, aunque están dentro de los parámetros de calidad óptimos.

Por tanto, a la vista de estos resultados no sería necesario prescindir de ninguna muestra ni recortar las lecturas obtenidas.

Seguidamente se muestran los gráficos "Sequence Duplication Levels" de cada una de las muestras:

<details>
<summary>SRR3480371 Sequence duplication levels</summary>

![SRR3480371 Sequence duplication levels](images/SRR3480371_sdl.png)
</details>

<details>
<summary>SRR3480372 Sequence duplication levels</summary>

![SRR3480372 Sequence duplication levels](images/SRR3480372_sdl.png)
</details>

<details>
<summary>SRR3480373 Sequence duplication levels</summary>

![SRR3480373 Sequence duplication levels](images/SRR3480373_sdl.png)
</details>

<details>
<summary>SRR3480374 Sequence duplication levels</summary>

![SRR3480374 Sequence duplication levels](images/SRR3480374_sdl.png)
</details>

En estos gráficos se observa que hay un alto porcentaje (entre el 20% y el 30%) de secuencias altamente repetidas (entre 10 y 500 veces). Como se trata de un análisis RNA-seq este comportamiento es totalmente normal, ya que lo que se está representando es la expresión de los genes. Por ejemplo, en la muestra *SRR3480373* se aprecia que del total de lecturas hay un 30% duplicadas más de 100 veces, lo que nos está indicando que los genes a los que corresponden esas lecturas se están sobreexpresando y generando, por tanto, más lecturas de esos tránscritos.

---
### Pregunta 2

Para indexar la referencia genómica y alinear las diferentes muestras contra ella, podemos ejecutar la siguiente instrucción:

```console
(rnaseq) ~/transcriptomica/Apartado1$ scripts/align.sh
```

Este script crea los directorios auxiliares y output necesarios y:
- Ejecuta el programa *hisat2-build* sobre el genoma de referencia para indexarlo.
- Ejecuta el programa *hisat2* sobre cada muestra para realizar el alineamiento, produciéndose en este paso un archivo *sam* por cada *fastq*.
- Ejecuta *samtools* para covertir a *bam* los archivos *sam*, ordenarlos e indexarlos de cara a preparar el posterior contaje de lecturas.

Al terminar, se habrá generado en el directorio *Apartado1* una estructura de archivos similar a la siguiente:

```console
(rnaseq) ~/transcriptomica/Apartado1$ tree
.
├── [840M]  hisat2
│   ├── [ 16M]  SRR3480371.bam
│   ├── [ 170]  SRR3480371.hisat2.summary
│   ├── [101M]  SRR3480371.sam
│   ├── [ 16M]  SRR3480371.sorted.bam
│   ├── [ 39K]  SRR3480371.sorted.bam.bai
│   ├── [ 21M]  SRR3480372.bam
│   ├── [ 170]  SRR3480372.hisat2.summary
│   ├── [141M]  SRR3480372.sam
│   ├── [ 21M]  SRR3480372.sorted.bam
│   ├── [ 41K]  SRR3480372.sorted.bam.bai
│   ├── [ 34M]  SRR3480373.bam
│   ├── [ 172]  SRR3480373.hisat2.summary
│   ├── [233M]  SRR3480373.sam
│   ├── [ 33M]  SRR3480373.sorted.bam
│   ├── [ 41K]  SRR3480373.sorted.bam.bai
│   ├── [ 26M]  SRR3480374.bam
│   ├── [ 170]  SRR3480374.hisat2.summary
│   ├── [174M]  SRR3480374.sam
│   ├── [ 26M]  SRR3480374.sorted.bam
│   └── [ 44K]  SRR3480374.sorted.bam.bai
── [479M]  input
│   ├── [124M]  REF
│   │   ├── [ 17M]  chr21.1.ht2
│   │   ├── [9.6M]  chr21.2.ht2
│   │   ├── [ 476]  chr21.3.ht2
│   │   ├── [9.6M]  chr21.4.ht2
│   │   ├── [ 17M]  chr21.5.ht2
│   │   ├── [9.7M]  chr21.6.ht2
│   │   ├── [  12]  chr21.7.ht2
│   │   ├── [   8]  chr21.8.ht2
├── [ 36K]  log
│   ├── [ 23K]  hisat2-build.log
│   ├── [ 170]  hisat2.SRR3480371.log
│   ├── [ 170]  hisat2.SRR3480372.log
│   ├── [ 172]  hisat2.SRR3480373.log
│   ├── [ 170]  hisat2.SRR3480374.log
│   ├── [   0]  samtools.SRR3480371.log
│   ├── [   0]  samtools.SRR3480372.log
│   ├── [   0]  samtools.SRR3480373.log
│   └── [   0]  samtools.SRR3480374.log
```
El programa *hisat2* es invocado para alinear las lecturas al genoma de referencia de la siguiente manera:

```console
hisat2 --new-summary --summary-file $HISATOUT/${sid}.hisat2.summary \
       --rna-strandness R --seed 123 --phred33 -p 6 -k 1 -x input/REF/chr21 \
       -U $DATADIR/${sid}.chr21.fastq -S $HISATOUT/${sid}.sam 2> $LOGDIR/hisat2.${sid}.log
```
Donde:
* *HISATOUT*: Indica el directorio de salida.
* *sid*: Es el nombre de la muestra extraído desde los ficheros origen (SRR3480371, ...).
* *new-summary*: Indica que el resultado se almacenará en un formato más fácilmente computable.
* *summary-file*: Nombre del fichero de salida.
* *rna-strandness*: Indica información sobre la hebra de ADN origen. En este caso se deduce del nombre del archivo de la muestra (SRR) que contiene información sobre la hebra en dirección **R**everse.
* *seed*: Fija la semilla para la generación de números aleatorios para que el resultado sea reproducible.
* *phred33*: Indica que las calidades están en el formato Phred+33 (por defecto).
* *p*: Indica el número de hilos de CPU a utilizar en el alineamiento.
* *k*: Indica el número de alineamientos a reportar por lectura. Poniéndolo a 1, indicamos que solo queremos reportar el alineamiento principal.
* *x*: Prefijo de los archivos de índice del genoma de referencia (obtenido con *hisat2-build*).
* *U*: Fichero fastq con las lecturas *unpaired*. En caso de lecturas *paired* usaríamos los parámetros *-1* y *-2*.
* *S*: Fichero en formato *sam* con el alineamiento realizado.

Los logs de la ejecución se guardarán en un archivo *hisat2.sid.log* por cada muestra.

A continuación se muestran los porcentajes de alineamiento obtenidos de cada una de las muestras, que se pueden consultar en los ficheros *sid.hisat2.summary* del directorio de salida:

<details>
<summary>Porcentajes de alineamiento para la muestra SRR3480371</summary>

```
HISAT2 summary stats:
	Total reads: 467560
		Aligned 0 time: 1111 (0.24%)
		Aligned 1 time: 466449 (99.76%)
		Aligned >1 times: 0 (0.00%)
	Overall alignment rate: 99.76%
```
</details>

<details>
<summary>Porcentajes de alineamiento para la muestra SRR3480372</summary>

```
HISAT2 summary stats:
	Total reads: 648121
		Aligned 0 time: 1334 (0.21%)
		Aligned 1 time: 646787 (99.79%)
		Aligned >1 times: 0 (0.00%)
	Overall alignment rate: 99.79%
```
</details>

<details>
<summary>Porcentajes de alineamiento para la muestra SRR3480373</summary>

```
HISAT2 summary stats:
	Total reads: 1068839
		Aligned 0 time: 1557 (0.15%)
		Aligned 1 time: 1067282 (99.85%)
		Aligned >1 times: 0 (0.00%)
	Overall alignment rate: 99.85%
```
</details>

<details>
<summary>Porcentajes de alineamiento para la muestra SRR3480374</summary>

```
HISAT2 summary stats:
	Total reads: 800203
		Aligned 0 time: 1823 (0.23%)
		Aligned 1 time: 798380 (99.77%)
		Aligned >1 times: 0 (0.00%)
	Overall alignment rate: 99.77%
```
</details>

> NOTA: Al haber especificado el parámetro *-k 1* no se obtienen lecturas alineadas más de 1 vez.

Los porcentajes de alineamiento varían entre el 99.76% y el 99.85% siendo unos resultados muy buenos.

### Pregunta 3

Por último, se genera el contaje de lecturas para la obtención de dos ficheros: uno con las cuentas crudas (*raw*) y otro con las cuentas normalizadas. Para generarlos se usará el programa *htseq* y una adaptación del script R *DESeq2_create_counts.R* visto en clase.

Al igual que en los casos anterior, podemos ejecutar este paso en concreto mediante la instrucción:

```console
(rnaseq) ~/transcriptomica/Apartado1$ scripts/count.sh
```

Este script crea los directorios auxiliares y output necesarios y:
- Ejecuta el programa *htseq* sobre los ficheros *bam* (ordenados e indexados) del paso anteior para generar archivos con extensión *htseq*.
- Invoca al script R *DESeq2_create_counts.R* que:
  * Leerá los ficheros *htseq* generados
  * Creará un DataSet DESeq mediante la función *DESeqDataSetFromHTSeqCount* (se especifica un diseño nulo *(design = ~1)* ya que no conocemos qué muestras son control y cuales no).
  * Normaliza la matriz de cuentas mediante el método *estimateSizeFactors*.
  * Genera los archivos *rawcounts.tsv* y *normalizedcounts.tsv* en el directorio de salida.

> NOTA: En este paso, no se eliminan los genes cuya suma de cuentas en las muestras sea menor que 10, aunque se indica en el código del script R cómo se haría si fuera necesario.  

Al terminar, se habrá generado en el directorio *Apartado1* una estructura de archivos similar a la siguiente:

```console
(rnaseq) ~/transcriptomica/Apartado1$ tree
.
├── [108K]  htseq
│   ├── [ 26K]  SRR3480371.htseq
│   ├── [ 26K]  SRR3480372.htseq
│   ├── [ 26K]  SRR3480373.htseq
│   └── [ 26K]  SRR3480374.htseq
├── [ 36K]  log
│   ├── [ 207]  htseq.SRR3480371.log
│   ├── [ 279]  htseq.SRR3480372.log
│   ├── [ 425]  htseq.SRR3480373.log
│   ├── [ 351]  htseq.SRR3480374.log
│   ├── [2.5K]  r.log
├── [1.5M]  output
│   ├── [ 35K]  normalizedcounts.tsv
│   └── [ 17K]  rawcounts.tsv

```
El programa *htseq* es invocado para efectuar el contaje de lecturas de cada muestra de la siguiente manera:

```console
htseq-count --format=bam --stranded=reverse --mode=intersection-nonempty \
            --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name \
            $HISATOUT/${sid}.sorted.bam $REFFILE > $HTSEQOUT/${sid}.htseq 2> $LOGDIR/htseq.${sid}.log
```
Donde:
* *HTSEQOUT*: Indica el directorio de salida donde se guardarán los ficheros de salida con el contaje de lecturas por gen.
* *sid*: Es el nombre de la muestra extraído desde los ficheros origen (SRR3480371, ...).
* *format*: Indica que el formato de entrada es *bam*. Se deduce del fichero de entrada, por lo que es un parámetro *deprecated*.
* *stranded*: Indica información sobre la hebra de ADN origen. Debe coincidir con la indicada en *hisat2*.
* *mode*: Indica cómo proceder en caso de que una lectura solape dos *features* (genes). Debajo se muestra una imagen con una explicación visual de los distintos modos de contaje. Al no especificarse explícitamente, el parámetro *nonunique* tomará el valor por defecto *none* y no se contarán las lecturas ambiguas.
* *minaqual*: Saltará las lecturas con un valor de calidad menor que 10 (especificado en la columna *MAPQ* del fichero *bam*).
* *type*: Indica el tipo de *feature* a usar, ignorando otros tipos. Este parámetro viene indicado en la tercera columna del fichero *GTF* de referencia a usar. En nuestro caso, para RNA-seq usaremos el tipo *exon*.
* *idattr*: Indica la columna del fichero *GTF* que se usará como feaure ID, en nuestro caso se usará el *gene_id*.
* *additional-attr*: Indica otras features que queramos añadir al fichero de salida, como por ejemplo el *gene_name*.
* *HISATOUT/${sid}.sorted.bam*: Indica el fichero *bam* de entrada (generado en el paso anterior con *hisat2* y ordenado e indexado con *samtools*).
* *REFFILE*: Fichero *GTF* con las features que queramos usar.

<details>
<summary>Modos de contaje de lecturas</summary>

![Modos de contaje de lecturas](images/count_modes.png)
</details>

Los logs de la ejecución se guardarán en un archivo *htseq.sid.log* por cada muestra.
