#!/bin/bash

# Create output dir
mkdir -p output

# Check if the required files exits
if [[ ! -f output/dpn24h.rnk || ! -f output/dpn48h.gmt  ]]; then
    echo "Los ficheros de entrada no existen. Ejecuta el scroipt Rmd para generarlos."
    exit 1;
fi

# Run GSEA from command line
GSEA_Linux_4.2.2/gsea-cli.sh GSEAPreranked -gmx output/dpn48h.gmt -collapse No_Collapse -mode Abs_max_of_probes -norm meandiv -nperm 1000 -rnd_seed timestamp -rnk output/dpn24h.rnk -scoring_scheme weighted -rpt_label DPN_48h_vs_24h -create_svgs false -include_only_symbols true -make_sets true -plot_top_x 20 -set_max 500 -set_min 15 -zip_report false -out output/
