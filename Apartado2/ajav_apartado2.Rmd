---
title: "Trabajo Transcriptómica Curso 2021 – 2022"
author: "Antonio José Arenas Valbuena"
output:
  html_document:
    df_print: paged
  pdf_document: default
---

# Enunciado Apartado 2

Disponemos de 24 cultivos primarios de tumores paratiroideos negativos para receptores de estrógenos alfa (ERα). Las muestras, procedentes de 4 pacientes diferentes, se han tratado con dos fármacos diferentes: diarilpropionitrilo (DPN) o 4-hidroxitamoxifeno (OHT) a 24h o 48h. El DPN es un agonista del ERα mientras que el OHT es un inhibidor competitivo de los receptores de estrógenos. Dicho dataset consta de los siguientes ficheros:

a.  Matriz de cuentas crudas.
b.  Data frame con los metadatos asociados al experimento.

## Pregunta 1

¿Qué genes se encuentran diferencialmente expresados entre las muestras pertenecientes al grupo tratado con OHT con respecto al control tras 24h? ¿Y en el caso de las muestras tratadas con DPN, también tras 24h? Como parte de la respuesta a esta pregunta, podéis entregar una o más tablas adjuntas donde se incluyan los genes diferencialmente expresados, indicando el criterio o los criterios que habéis seguido para filtrar los resultados, así como cualquier otro gráfico o gráficos que hayáis elaborado durante el análisis.

------------------------------------------------------------------------

### Resolución

En primer lugar, cargamos las librerías que se van a usar para la resolución del ejercicio. Deberán estar previamente instaladas en el sistema, pudiéndose usar el mismo entorno "rnaseq" que en el apartado 1, y se limpia el entorno de ejecución.

```{r message=FALSE, warning=FALSE}
# Carga de librerías
library("DESeq2")
library("pheatmap")
library("RColorBrewer")
library(org.Hs.eg.db)
library("EnhancedVolcano")

# Limpieza de entorno
rm(list=ls())
```

A continuación se cargan los ficheros proporcionados en el enunciado. Deberán estar en un subdirectorio *input*. Se usa el parámetro *row_names* para indicar que la primera columna de los archivos tsv se usará como nombre de las filas del dataframe.

```{r}
# Lectura del fichero con las cuentas crudas.
cts <- as.matrix (read.csv ('input/rawcounts.tsv', sep = '\t', row.names = 1))
# Lectura del fichero con los metadatos.
coldata <- read.csv ('input/metadata.tsv', sep = '\t', row.names = 1)

# Se comprueba que los datos se han cargado correctamente visualizando las primeras líneas.
head(cts)
head(coldata)
```

Factorizamos las columnas *patient* y *agent* de los metadatos.

Antes de construir el dataset DESeq comprobamos que metadatos y matriz de cuentas son consistentes, esto es, que las columnas de la matriz de cuentas crudas coinciden con las filas del dataframe de metadatos.

```{r}
coldata$patient<-factor(coldata$patient)
coldata$agent<-factor(coldata$agent)

all(rownames(coldata) %in% colnames(cts))
all(rownames(coldata) == colnames(cts))
```

Se construye el dataset DESeq con todos los datos del experimento.

```{r}
ddsFull <- DESeqDataSetFromMatrix(countData = cts,
                                  colData = coldata,
                                  design = ~patient + agent)
```

A continuación manipulo el dataset para quedarme con los datos tomados a las 24 horas, factorizo las columnas necesarias y establezco como nivel de referencia del factor el correspondiente a las muestras de control.

También elimino del dataset aquellos genes cuya suma de lecturas en todas las muestras es inferior a 10.

```{r}
# Selección del dataset con datos a las 24 horas.
dds24h <- ddsFull[, colData(ddsFull)$time == "24h"]

# Refactorización y ajuste de niveles de referencia.
dds24h$patient<-factor(dds24h$patient)
dds24h$agent  <-factor(dds24h$agent)
dds24h$time  <-factor(dds24h$time)

dds24h$agent <- relevel(dds24h$agent, "Control")

## Prefiltrado. Se eliminan los genes cuya suma de lecturas en todas las muestras es inferior a 10.
dds24h <- dds24h[rowSums(counts(dds24h)) >= 10, ]

colData(dds24h)
```

#### Análisis exploratorio

Como paso previo al análisis DESeq se hará un análisis exploratorio para detectar posibles efectos *batch* o datos erróneos. Para ello usaremos gráficos PCA y clustering.

```{r}
# Con la función VST se normaliza y estabiliza la varianza de las cuentas, paso necesario para PCA.
vsd24h <- vst(dds24h, blind = TRUE)

# Mostramos el gráfico Principal Component Analysis
plotPCA(vsd24h, intgroup = c("patient","agent"))

# Calculamos las distancias a partir de las cuentas normalizadas y variance-stabilized (vst)
sampleDists24h <- dist(t(assay(vsd24h)))

# Generamos un gráfico heatmap
sampleDistMatrix24h <- as.matrix(sampleDists24h)
rownames(sampleDistMatrix24h) <- paste(vsd24h$patient, vsd24h$agent, sep="-")
colors <- colorRampPalette( rev(brewer.pal(9, "YlOrBr")) )(255)
pheatmap(sampleDistMatrix24h,
         clustering_distance_rows=sampleDists24h,
         clustering_distance_cols=sampleDists24h,
         col=colors)
```

Se puede ver que la muestra de control del paciente 4 presenta problemas, tanto en el PCA como en el cluster, ya que los datos de expresión génica de la muestra no son parecidos a los datos del resto de muestras del paciente, como en los demás casos. Dicho de otra forma, esta muestra presenta causas de variabilidad en la expresión que no son explicados por el modelo paciente + agente.

Vistos estos resultados, se procede a eliminar las muestras del paciente 4 del estudio, ya que al presentar problemas precisamente la muestra de control, no podrán compararse las muestras de los tratamientos con el control y podrían producirse resultados incorrectos. Una vez elimindo el paciente, calculo de nuevo los objetos anteriores y vuelvo a generar las gráficas exploratorias.

```{r}
coldata_filtered <- coldata[coldata$patient!=4,]
cts_filtered <- cts[,c(rownames(coldata_filtered))]

coldata_filtered$patient<-factor(coldata_filtered$patient)
coldata_filtered$agent<-factor(coldata_filtered$agent)

ddsFull_filtered <- DESeqDataSetFromMatrix(countData = cts_filtered,
                                           colData = coldata_filtered,
                                           design = ~patient + agent)

dds24h <- ddsFull_filtered[, colData(ddsFull_filtered)$time == "24h"]
dds24h$patient<-factor(dds24h$patient)
dds24h$agent  <-factor(dds24h$agent)
dds24h$agent <- relevel(dds24h$agent, "Control")
dds24h <- dds24h[rowSums(counts(dds24h)) >= 10, ]

vsd24h <- vst(dds24h, blind = TRUE)
plotPCA(vsd24h, intgroup = c("patient","agent"))

sampleDists24h <- dist(t(assay(vsd24h)))
sampleDistMatrix24h <- as.matrix(sampleDists24h)
rownames(sampleDistMatrix24h) <- paste(vsd24h$patient, vsd24h$agent, sep="-")
colors <- colorRampPalette( rev(brewer.pal(9, "YlOrBr")) )(255)
pheatmap(sampleDistMatrix24h,
         clustering_distance_rows=sampleDists24h,
         clustering_distance_cols=sampleDists24h,
         col=colors)
```

## Análisis de expresión diferencial

En vista a estos resultados, se procede a ejecutar el DESeq y representar los resultados.

```{r Resultados DEA}
# Ejecutamos el análisis DESeq
dds24h <- DESeq(dds24h, test = "Wald")

## Vamos a verificar cómo ha quedado la estimación de la dispersión 
plotDispEsts(dds24h)

## Exploremos cómo quedan los cambios de fold entre condiciones con respecto
## a las cuentas normalizadas
plotMA(dds24h)

# Obtenemos la lista de genes DEG para el tratamiento OHT
resultsOHT_24h <- results(object = dds24h,
                          contrast = c("agent", "OHT", "Control"),
                          alpha = 0.05,
                          pAdjustMethod = "BH"
)

# Obtenemos la lista de genes DEG para el tratamiento DPN
resultsDPN_24h <- results(object = dds24h,
                          contrast = c("agent", "DPN", "Control"),
                          alpha = 0.05, 
                          pAdjustMethod = "BH"
)

# Mostramos un resumen del resultado para el tratamiento OHT a las 24 horas
summary(resultsOHT_24h)

# Mostramos un resumen del resultado para el tratamiento DPN a las 24 horas
summary(resultsDPN_24h)

#  Traducimos el nombre de los genes de nomenclatura Ensembl gene IDs a símbolos
symbols <- mapIds(org.Hs.eg.db, keys = rownames(cts), column = c('SYMBOL'), keytype = 'ENSEMBL')

resultsOHT_24h$geneSym <- symbols[match(rownames(resultsOHT_24h), names(symbols))]
resultsDPN_24h$geneSym <- symbols[match(rownames(resultsDPN_24h), names(symbols))]

# Mostramos los genes DE con un p-valor ajustado inferior a 0.05
resultsOHT_24h[!is.na(resultsOHT_24h$padj) & resultsOHT_24h$padj < 0.05,]

resultsDPN_24h[!is.na(resultsDPN_24h$padj) & resultsDPN_24h$padj < 0.05,]

# Mostramos el nombre de los genes DE para DPN a las 24h
resultsDPN_24h[!is.na(resultsDPN_24h$padj) & resultsDPN_24h$padj < 0.05,]$geneSym

```

Este análisis nos muestra que en el caso del tratamiento con OHT no se encuentran genes DE con el nivel de significancia requerido (padj \< 0.05), mientras que en el caso del tratamiento con DPN sí encontramos 22 genes DE.

Para ver las diferencias de expresión se va a mostrar un volcano plot con estos resultados estableciendo un umbral más laxo en el pvalor. En el volcano plot de "DPN vs Control 24h" se puede apreciar que pocos genes cumplen a la vez las restricciones sobre el pvalor y el LFC.

```{r}
EnhancedVolcano(resultsOHT_24h,
  lab = resultsOHT_24h$geneSym,
  x = 'log2FoldChange',
  y = 'pvalue',
  xlim = c(-6,6),
  ylim = c(0,10),
  title = 'OHT vs Control (24h)',
  pCutoff = 10e-3,
  pointSize = 1.0,
  labSize = 4.0,
  legendPosition = 'right',
  legendLabSize = 10,
  legendIconSize = 1.5,
)

EnhancedVolcano(resultsDPN_24h,
  lab = resultsDPN_24h$geneSym,
  x = 'log2FoldChange',
  y = 'pvalue',
  xlim = c(-6,6),
  ylim = c(0,10),
  title = 'DPN vs Control (24h)',
  pCutoff = 10e-3,
  pointSize = 1.0,
  labSize = 4.0,
  legendPosition = 'right',
  legendLabSize = 10,
  legendIconSize = 1.5,
)
```

## Pregunta 2

Genera dos firmas con los 100 genes más up-/down-regulados en las células tratadas con DPN durante 48h. ¿Están estas firmas enriquecidas en las células tratadas con DPN durante 24h? ¿Qué conclusiones extraes de este resultado? Incluir una tabla con los resultados del análisis, destacando las columnas con los valores utilizados para extraer vuestras conclusiones. También incluir los gráficos característicos de este tipo de análisis.

------------------------------------------------------------------------

### Resolución

Primero, se obtienen los DEG para el test DPN vs Control a las 48 horas, con un procedimiento similar al anterior.

```{r}
# Selección del dataset con datos a las 24 horas.
dds48h <- ddsFull[, colData(ddsFull)$time == "48h"]

# Refactorización y ajuste de niveles de referencia.
dds48h$patient<-factor(dds48h$patient)
dds48h$agent  <-factor(dds48h$agent)

dds48h$agent <- relevel(dds48h$agent, "Control")

## Prefiltrado. Se eliminan los genes cuya suma de lecturas en todas las muestras es inferior a 10.
dds48h <- dds48h[rowSums(counts(dds48h)) >= 10, ]

vsd48h <- vst(dds48h, blind = TRUE)

# Mostramos el gráfico Principal Component Analysis
plotPCA(vsd48h, intgroup = c("patient","agent"))

## Calculamos las distancias a partir de las cuentas normalizadas y variance-stabilized (vst)
sampleDists48h <- dist(t(assay(vsd48h)))

sampleDistMatrix48h <- as.matrix(sampleDists48h)
rownames(sampleDistMatrix48h) <- paste(vsd48h$patient, vsd48h$agent, sep="-")
#colnames(sampleDistMatrix24h) <- NULL
colors <- colorRampPalette( rev(brewer.pal(9, "YlOrBr")) )(255)
pheatmap(sampleDistMatrix48h,
         clustering_distance_rows=sampleDists48h,
         clustering_distance_cols=sampleDists48h,
         col=colors)
```

En este caso, no es necesario eliminar ninguna muestra.

```{r Resultados DEA 48h}
# Ejecutamos el análisis DESeq
dds48h <- DESeq(dds48h, test = "Wald")

## Vamos a verificar cómo ha quedado la estimación de la dispersión 
plotDispEsts(dds48h)

## Exploremos cómo quedan los cambios de fold entre condiciones con respecto
## a las cuentas normalizadas
plotMA(dds48h)

## Obtengamos nuestra lista de genes DEG
resultsDPN_48h <- results(object = dds48h,
                          contrast = c("agent", "DPN", "Control"),
                          alpha = 0.05,
                          pAdjustMethod = "BH"
)

# Mostramos un resumen del análisis
summary(resultsDPN_48h)

# Mostramos los genes DE con un p-valor ajustado inferior a 0.05
resultsDPN_48h[!is.na(resultsDPN_48h$padj) & resultsDPN_48h$padj < 0.05,]

# Volcano plot
EnhancedVolcano(resultsDPN_48h,
  lab = rownames(resultsDPN_48h),
  x = 'log2FoldChange',
  y = 'pvalue',
  xlim = c(-6,6),
  ylim = c(0,10),
  title = 'DPN vs Control (48h)',
  pCutoff = 10e-3,
  pointSize = 1.0,
  labSize = 4.0,
  legendPosition = 'right',
  legendLabSize = 10,
  legendIconSize = 1.5,
)
```

En este análisis se obtienen 129 genes DE (52 up-regulados y 77 down-regulados). Para analizar mejor estos resultados, vamos a exportar los datos y a ejecutar un análisis GSEA.

```{r message=FALSE, warning=FALSE}

# Es importante efectuar un proceso de *LFC shrink* para poder comparar ambos resultados. 
# Este proceso es también necesario ya que los genes con un contaje de lecturas basjas tienden 
# a tener una gran variabilidad de LFC, por lo que pueden distorsionar el resultado.
resultsDPN_48h.ape <- lfcShrink(dds48h, coef = "agent_DPN_vs_Control", type = "apeglm", res = resultsDPN_48h)
resultsDPN_24h.ape <- lfcShrink(dds24h, coef = "agent_DPN_vs_Control", type = "apeglm", res = resultsDPN_24h)

# En los siguientes gráficos MA se puede apreciar como tras el lfcShrink los genes con bajo contaje de lecturas
# tienen un LFC cercano a 0, mientras que los genes con alto contaje de lecturas presentan aproximadamente el mismo LFC
plotMA(resultsDPN_48h, ylim = c(-3, 3))
plotMA(resultsDPN_48h.ape, ylim = c(-3, 3))

plotMA(resultsDPN_24h, ylim = c(-3, 3))
plotMA(resultsDPN_24h.ape, ylim = c(-3, 3))

```

Creamos los archivos de salida en el formato requerido por GSEA.

```{r message=FALSE, warning=FALSE}
library ("dplyr")

# Función para convertir un dataframe a un archivo gmt para GSEA
output.gmt <- function(l, filename) {
  if(file.exists(filename)) {
    warning(paste("Removing previous", filename, "file."))
    file.remove(filename)
  }
  invisible(lapply(1:length(l), function(i) {
    cat(c(names(l)[i], "na", l[[i]], "\n"), sep = "\t", file = filename,
        append = TRUE)
  }))
}

# Creamos los archivos con el listado de genes y LFC
rnk24h <- data.frame(Feature = rownames(resultsDPN_24h.ape), LFC = resultsDPN_24h.ape$log2FoldChange)
rnk48h <- data.frame(Feature = rownames(resultsDPN_48h.ape), LFC = resultsDPN_48h.ape$log2FoldChange)

# Creamos el archivo con la lista de genes y sus niveles de expresión a las 24 horas.
write.table(rnk24h, file = "output/dpn24h.rnk", sep = "\t", quote = FALSE, col.names = FALSE, row.names = FALSE)

# Generamos dos vectores con los 100 genes más up/down regulados y los ordenamos por LFC
up100_48h <- rnk48h %>% arrange(desc(LFC))
up100_48h <- up100_48h$Feature[1:100]

down100_48h <- rnk48h %>% arrange(LFC)
down100_48h <- down100_48h$Feature[1:100]

# Generamos una lista con los dos vectores anteriores
geneset_48h <- list(up_dpn = up100_48h, down_dpn = down100_48h)

# Generamos el archivo gmt para GSEA
output.gmt(geneset_48h, filename = "output/dpn48h.gmt")

```

Como alternativa para un GSEA rápido, usaremos el paquete fgsea de Bioconductor. En vez de leer los archivos generados en el paso anterior, manipulamos los dataframes con los datos que ya tenemos y ejecutamos GSEA.

```{r warning=FALSE}
# Cargamos las librerías necesarias (se encuentran en el ambiente rnaseq usado durante todo el trabajo)
library(fgsea)
library(data.table)
library(ggplot2)

# Comandos para leer los datos desde fichero
#ranks <- read.table("dpn24h.rnk", header=FALSE, colClasses = c("character", "numeric"))
#pathways <- gmtPathways("dpn48h.gmt")

# Transformamos el dataset rnk24h en un "named vector" con los genes y sus niveles de expresión
rank  <- rnk24h[,2]
names(rank) = rnk24h[,1]

# Ejecutamos el GSEA
fgseaRes <- fgsea(pathways = geneset_48h, 
                  stats    = rank,
                  minSize  = 15,
                  maxSize  = 500)

# Mostramos un resumen del GSEA
fgseaRes$pathway
fgseaRes$ES
fgseaRes$NES

# Generamos los gráficos de enriquecimiento para los dos conjuntos de genes (up y down)
plotEnrichment(pathway = geneset_48h[["up_dpn"]], rank) + labs(title="DPN 48h vs 24h")
plotEnrichment(pathway = geneset_48h[["down_dpn"]], rank) + labs(title="DPN 48h vs 24h")


```

Las gráficas obtenidas, así como los valores ES y NES son muy similares (aunque no iguales) a los obtenidos con la aplicación de escritorio GSEA. Los gráficos muestran como, tanto los genes up-regulados como los down-regulados en el estudio a las 48 horas, también se encuentran expresados a las 24 horas.

En el archivo README.md del repositorio muestro los resultados del análisis con GSEA de escritorio, así como una respuesta a las preguntas del enunciado.
