# Trabajo de Trancriptómica del Master de Bioinformática 2021/2022

## Apartado2

Este directorio contiene los scripts y ficheros de input necesarios para realizar el análisis de expresión diferencial (DEA) solicitado en el enunciado. La estructura de archivos es la siguiente:

* README.md (este archivo): Contiene las instrucciones para ejecutar el DEA. Las respuestas a las preguntas planteadas en el enunciado y la interpretación de los resultados obtenidos se encuentran en el archivo R markdown.
* images: Contiene los gráficos a analizar.
* input: Archivos de entrada.
    + metadata.tsv: Metadatos asociados al experimento.
    + rawcounts.tsv: Fichero de cuentas crudas.
* GESA_Linux_4.2.2: Contiene el programa GSEA descargado del [Broad Institute](https://www.gsea-msigdb.org/gsea/index.jsp).
* ajav_apartado2.Rmd: Fichero en formato R-Markdown que explica el procedimiento seguido para el análisis de expresión diferencial y contiene el análisis de los resultados obtenidos.
* ajav_apartado2.html: Fichero que contiene el resultado de la ejecución del archivo anterior listo para ser visualizado en un navegador.
* run_gsea.sh: Script para la ejecución de GSEA desde la línea de comandos.

### Ejecución del DEA

El script *ajav_apartado2.Rmd* se ejecutará desde la herramienta **RStudio** con el ambiente *rnaseq* activado desde el directoio "Apartado2" del repositorio.

```console
(rnaseq) ~/transcriptomica/Apartado2$ rstudio &
```

A continuación se irán ejecutando los distintos *chunks* incluidos para realizar el DEA paso a paso. El análisis de los resultados se realiza en el archivo *Rmd*, haciéndose a continuación un resumen de los puntos más relevantes.

### Pregunta 1

Tras cargar las librerías necesarias y limpiar el entorno, se procede a leer los ficheros de entrada y a construir el dataset DESeq2. Para este análisis, además de los filtros relacionados con el análisis en si (realizados sobre el rango de tiempo), se eliminan aquellos genes cuya suma de cuentas en todas las muestras a analizar no sea igual o superior a 10.

En el análisis exploratorio a 24 horas, se observa que la muestra de control del paciente 4 contiene datos de expresión muy dispares a las otras muestras del paciente, por lo que se decide eliminar al paciente 4 del estudio.

<details>
<summary>Se incluyen a continuación las gráficas PCA y heatmap con todas las muestras (clic para expadnir):</summary>

![PCA](images/PCA_1.png)
![Heatmap](images/heatmap_1.png)
</details>

Seguidamente, se vuelve a crear el dataset DESeq2 sin los datos del paciente, observándose que tanto el PCA como el heatmap muestran comportamientos normales.

<details>
<summary>Se incluyen a continuación las gráficas PCA y heatmap sin las muestras del paciente 4 (clic para expadnir):</summary>

![PCA](images/PCA_2.png)
![Heatmap](images/heatmap_2.png)
</details>


En el *chunk* "Resultados DEA" se ejecuta el DEA llamando a la función *DESeq* y se muestran distintos resultados. Los más significativos son los siguientes:

- **No hay genes diferencialmente expresados** entre las muestras pertenecientes al grupo tratado con **OHT** con respecto al control tras 24h.

- **Hay 22 genes diferencialmente expresados** entre las muestras pertenecientes al grupo tratado con **DPN** con respecto al control tras 24h, que son:

```
> summary(resultsDPN_24h)

out of 21085 with nonzero total read count
adjusted p-value < 0.05
LFC > 0 (up)       : 12, 0.057%
LFC < 0 (down)     : 10, 0.047%
outliers [1]       : 0, 0%
low counts [2]     : 8176, 39%
(mean count < 37)
[1] see 'cooksCutoff' argument of ?results
[2] see 'independentFiltering' argument of ?results
```

```
"ENSG00000044574" "ENSG00000076706" "ENSG00000078081" "ENSG00000085063"
"ENSG00000099194" "ENSG00000100219" "ENSG00000101335" "ENSG00000103335"
"ENSG00000111328" "ENSG00000113494" "ENSG00000120129" "ENSG00000124406"
"ENSG00000127022" "ENSG00000127249" "ENSG00000128590" "ENSG00000135069"
"ENSG00000138449" "ENSG00000141522" "ENSG00000147003" "ENSG00000166598"
"ENSG00000196924" "ENSG00000227827"
```
O traducido a símbolos:
```
"HSPA5"          "MCAM"          "LAMP3"          "CD59"
"SCD"            "XBP1"          "MYL9"           "PIEZO1"
"CDK2AP1"        "PRLR"          "DUSP1"          "ATP8A1"
"CANX"           "ATP13A4"       "DNAJB9"         "PSAT1"
"SLC40A1"        "ARHGDIA"       "CLTRN"          "HSP90B1"
"FLNA"           "PKD1P2"
```

### Pregunta 2

Análogamente al apartado anterior, se genera el dataset para el estudio de 48 horas y se ejecuta el análisis exploratorio correspondiente, no observándose en este caso, ningún problema reseñable en las muestras.

<details>
<summary>Se incluyen a continuación las gráficas PCA y heatmap para el estudio a 48 horas (clic para expadnir):</summary>

![PCA](images/PCA_3.png)
![Heatmap](images/heatmap_3.png)
</details>

Al ejecutar el análisis DE sobre estos datos, obtenemos que para el tratamiento **DPN** hay **129 genes diferencialmente expresados** (a un nivel de significación del pvalor ajustado del 5%).

```
> summary(resultsDPN_48h)

out of 21279 with nonzero total read count
adjusted p-value < 0.05
LFC > 0 (up)       : 52, 0.24%
LFC < 0 (down)     : 77, 0.36%
outliers [1]       : 0, 0%
low counts [2]     : 11964, 56%
(mean count < 131)
[1] see 'cooksCutoff' argument of ?results
[2] see 'independentFiltering' argument of ?results
```

Para determinar si estas firmas están enriquecidas en las células tratadas con DPN durante 24h, se utilizará la herramienta *GSEA* del *Broad Institute*, haciendo un análisis **GSEA preranked**. Para ello:
- Desde *RStudio* se preparan los resultados obtenidos en los análisis de los apartados anteriores y se generan los archivos necesarios para su posterior análisis GSEA, usando como base el procedimiento visto en clase.
- También desde *RStudio* se usará el paquete *fgsea* de *Bioconductor* para realizar el análisis GSEA preranked de una manera alternativa e integrada en RStudio, obteniéndose unos resultados muy similares en ambos casos.
- Se procederá a invocar a la herramienta GSEA por línea de comandos, tomando como entrada los ficheros generados anteriormente.

Los ficheros generados se almacenarán en el directorio *output* y son:
- **dpn24h.rnk**: Contiene la lista de genes obtenida en el análisis de 24 horas y su expresión (*log2 fold change*).
- **dpn48h.gmt**: Contendrá dos líneas con los resultados del DEA a las 48 horas, la primera con la lista de los genes más *up-regulados* y la segunda con la lista de los genes más *down-regulados*.

> NOTA: Es importante efectuar un proceso de *LFC shrink* para poder comparar ambos resultados. Este proceso es también necesario ya que los genes con un contaje de lecturas basjas tienden a tener una gran variabilidad de LFC, por lo que pueden distorsionar el resultado.

En los siguientes gráficos MA se puede apreciar como tras el lfcShrink los genes con bajo contaje de lecturas tienen un LFC cercano a 0, mientras que los genes con alto contaje de lecturas presentan aproximadamente el mismo LFC que anteriormente.

<details>
<summary>MA plot 48h antes del lfcShrink</summary>

![MA1](images/MA_1.png)
</details>

<details>
<summary>MA plot 48h después del lfcShrink</summary>

![MA2](images/MA_2.png)
</details>


En el análisis fGSEA (*fast gsea*) ejecutado en R se obtienen los siguientes resultados:

<details>
<summary>Enrichment plot para el conjunto de genes up-regulados</summary>

![FGSEA_UP](images/fgsea_up.png)
</details>

<details>
<summary>Enrichment plot para el conjunto de genes down-regulados</summary>

![FGSEA_DOWN](images/fgsea_down.png)
</details>

Las gráficas de enriquecimiento para los dos gene sets indican que **los genes más up/down regulados a las 48 horas están expresadas de manera similar a las 24 horas**.

Por ejemplo, en el gráfico para los genes más up-regulados, en el eje X se dispondrían los genes de la lista de 24 horas ordenada por LFC (de mayor a menor). Cuando existe una coincidencia entre el gene set de 48 horas y el de 24, se representa con una línea negra vertical en el eje X. Al haber una gran cantidad de estas líneas en la parte izquierda del eje X, eso nos indica que la mayoría de los genes up-regulados a las 48 horas están también up-regulados a las 24 horas.

Adicionalmente, se genera un *Enrichment Score (ES)* que indica cuándo se produce la máxima coincidencia entre los dos gene sets comparados, siendo un valor cercano a 1 el correspondiente al principio de la lista (LFC más alto) y -1 al final (LFC más bajo). Los genes a la izquierda (o respectivamente a la derecha) del *enrichment score* se denominan *Leading edge subset* y representan los genes que más contribuyen al cálculo del *ES*.

El parámetro *ES* se suele normalizar para efectuar comparaciones entre distintos análisis GSEA, denominándose *Normalized Enrichment Score (NES)*.

Los parámetros obtenidos en el análisis *fgsea* efectuado desde R son:

- Para los 100 genes más up-regulados:

| Parámetro  | Valor |
|-----|-----------|
| ES  | 0.9713359 |
| NES | 1.828125  |

Siendo los genes que forman parte del *Leading edge subset*:
```
> fgseaRes[2]$leadingEdge
[[1]]
 [1] "ENSG00000128590" "ENSG00000138449" "ENSG00000127249" "ENSG00000099194" "ENSG00000044574" "ENSG00000100219" "ENSG00000197594" "ENSG00000166598"
 [9] "ENSG00000159640" "ENSG00000164237" "ENSG00000173281" "ENSG00000151726" "ENSG00000114948" "ENSG00000123983" "ENSG00000173890" "ENSG00000123643"
[17] "ENSG00000092621" "ENSG00000179218" "ENSG00000155660" "ENSG00000151692" "ENSG00000114757" "ENSG00000206384" "ENSG00000070669" "ENSG00000226549"
[25] "ENSG00000211584" "ENSG00000198830" "ENSG00000221890" "ENSG00000138792" "ENSG00000157326" "ENSG00000134824" "ENSG00000171227" "ENSG00000158457"
```

- Para los 100 genes más down-regulados:

| Parámetro  | Valor |
|-----|-----------|
| ES  | -0.9402914 |
| NES | -1.762362  |

Siendo los genes que forman parte del *Leading edge subset*:
```
> fgseaRes[1]$leadingEdge
[[1]]
 [1] "ENSG00000111328" "ENSG00000120129" "ENSG00000105655" "ENSG00000101335" "ENSG00000227105" "ENSG00000133639" "ENSG00000143799" "ENSG00000168386"
 [9] "ENSG00000198959" "ENSG00000091137" "ENSG00000189060" "ENSG00000072110" "ENSG00000020577" "ENSG00000241878" "ENSG00000066468" "ENSG00000169429"
[17] "ENSG00000168014" "ENSG00000100906" "ENSG00000138772" "ENSG00000115461" "ENSG00000137331" "ENSG00000069849" "ENSG00000155330" "ENSG00000168542"
[25] "ENSG00000138685" "ENSG00000111799" "ENSG00000146376" "ENSG00000107796" "ENSG00000123200" "ENSG00000182836" "ENSG00000005108" "ENSG00000041982"
```

Para ejecutar la herramienta GSEA desde la línea de comando ejecutaremos el comando *run_gsea.sh* desde el directorio "Apartado2", habiendo generado los ficheros *rnk* y *gmt* requeridos en el paso anterior:

```console
(rnaseq) ~/transcriptomica/Apartado2$ ./run_gsea.sh
echo Using bundled JDK.
WARNING: package com.apple.laf not in java.desktop
WARNING: package com.sun.java.swing.plaf.windows not in java.desktop
[.....]
4699     [INFO  ] [thr-main] - Done FDR reports for positive phenotype
4907     [INFO  ] [thr-main] - Done FDR reports for negative phenotype
5024     [INFO  ] [thr-main] - Creating global reports ...
5024     [INFO  ] [thr-main] - Done all reports!!
Time taken: 2 secs
```
Se generará un subdirectorio dentro de *output* con el nombre *DPN_48h_vs_24h.GseaPreranked* seguido de un número indicando el *timestamp* de la ejecución.

Dentro de este directorio se encontrarán varios archivos con los informes generados, de los que se puede extraer la siguiente información:

<details>
<summary>Enrichment plot para el conjunto de genes up-regulados</summary>

![GSEA_UP](images/enplot_UP_DPN_1.png)
</details>

<details>
<summary>Enrichment plot para el conjunto de genes down-regulados</summary>

![GSEA_DOWN](images/enplot_DOWN_DPN_3.png)
</details>

Valores de los parámetros más reseñables:

- Para los 100 genes más up-regulados:

| Parámetro  | Valor |
|-----|-----------|
| ES  | 0.9713361 |
| NES | 1.8595972 |


- Para los 100 genes más down-regulados:

| Parámetro  | Valor |
|-----|-----------|
| ES  | -0.94029135 |
| NES | -1.7635314  |

<details>
<summary>Lista con los genes del leading edge subset más up-regulados (archivo UP_DPN.tsv)</summary>

```
NAME	SYMBOL	RANK IN GENE LIST	RANK METRIC SCORE	RUNNING ES	CORE ENRICHMENT
row_0   ENSG00000128590 4   0.5758358836174011      0.15974884  Yes
row_1   ENSG00000138449 10  0.34395837783813477     0.25504562  Yes
row_2   ENSG00000127249 11  0.33594828844070435     0.34835586  Yes
row_3   ENSG00000099194 12  0.3308427929878235      0.44024804  Yes
row_4   ENSG00000044574 13  0.323277086019516       0.53003883  Yes
row_5   ENSG00000100219 14  0.31146687269210815     0.6165493   Yes
row_6   ENSG00000197594 20  0.2785259485244751      0.6936721   Yes
row_7   ENSG00000166598 22  0.2582645118236542      0.7653579   Yes
row_8   ENSG00000159640 29  0.21158228814601898     0.82383937  Yes
row_9   ENSG00000164237 37  0.16975486278533936     0.8706555   Yes
row_10  ENSG00000173281 38  0.1667400449514389      0.91696787  Yes
row_11  ENSG00000151726 39  0.16510725021362305     0.96282667  Yes
row_12  ENSG00000114948 49  0.0048391614109277725   0.9637419   Yes
row_13  ENSG00000123983 52  0.004745154175907373    0.96496457  Yes
row_14  ENSG00000173890 57  0.00438764737918973     0.9659926   Yes
row_15  ENSG00000123643 77  0.0038321262691169977   0.9661516   Yes
row_16  ENSG00000092621 78  0.003801678540185094    0.9672075   Yes
row_17  ENSG00000179218 84  0.003668529214337468    0.9679882   Yes
row_18  ENSG00000155660 85  0.0036545514594763517   0.96900326  Yes
row_19  ENSG00000151692 98  0.003515570657327771    0.96940786  Yes
row_20  ENSG00000114757 107 0.0034040214959532022   0.96997213  Yes
row_21  ENSG00000206384 111 0.0033687411341816187   0.9707648   Yes
row_22  ENSG00000070669 139 0.0031050771940499544   0.9703406   Yes
row_23  ENSG00000226549 180 0.002887326292693615    0.9692365   Yes
row_24  ENSG00000211584 187 0.002843847032636404    0.96974045  Yes
row_25  ENSG00000198830 238 0.0026261017192155123   0.9680872   Yes
row_26  ENSG00000221890 240 0.0026233349926769733   0.9687682   Yes
row_27  ENSG00000138792 243 0.0026157277170568705   0.9693994   Yes
row_28  ENSG00000157326 249 0.002594349440187216    0.9698817   Yes
row_29  ENSG00000134824 256 0.002560935914516449    0.9703071   Yes
row_30  ENSG00000171227 260 0.002546527422964573    0.97087145  Yes
row_31  ENSG00000158457 266 0.0025308653712272644   0.9713361   Yes
row_32  ENSG00000168209 322 0.002318520098924637    0.96935916  No  
row_33  ENSG00000159307 384 0.002161789685487747    0.96705276  No  
row_34  ENSG00000226608 402 0.002128210151568055    0.9668338   No  
[ se omiten el resto al no formar parte del leading edge]
```
</details>

<details>
<summary>Lista con los genes del leading edge subset más down-regulados (archivo DOWN_DPN.tsv)</summary>

```
NAME	SYMBOL	RANK IN GENE LIST	RANK METRIC SCORE	RUNNING ES	CORE ENRICHMENT
[ se omiten los primeros al no formar parte del leading edge]
row_60  ENSG00000181264 20130   -0.0016666284063830972  -0.92930895 No  
row_61  ENSG00000196154 20145   -0.001677777268923819   -0.92905885 No  
row_62  ENSG00000087053 20363   -0.0019199518719688058  -0.9383499  No  
row_63  ENSG00000164597 20403   -0.001976033905521035   -0.93912804 Yes
row_64  ENSG00000075539 20405   -0.0019773303065449     -0.9380947  Yes
row_65  ENSG00000124177 20432   -0.0020197469275444746  -0.93822944 Yes
row_66  ENSG00000111907 20449   -0.002054001670330763   -0.93786895 Yes
row_67  ENSG00000102547 20472   -0.0021063238382339478  -0.9377657  Yes
row_68  ENSG00000041982 20526   -0.0022239936515688896  -0.93907547 Yes
row_69  ENSG00000005108 20527   -0.002225505653768778   -0.93785876 Yes
row_70  ENSG00000182836 20530   -0.002233268925920129   -0.9367331  Yes
row_71  ENSG00000123200 20603   -0.002385315950959921   -0.93886006 Yes
row_72  ENSG00000107796 20609   -0.002395456889644265   -0.93778867 Yes
row_73  ENSG00000146376 20610   -0.002396462019532919   -0.9364785  Yes
row_74  ENSG00000111799 20631   -0.0024524028412997723  -0.93609077 Yes
row_75  ENSG00000138685 20720   -0.0026989905163645744  -0.9388087  Yes
row_76  ENSG00000168542 20751   -0.002800539368763566   -0.9387072  Yes
row_77  ENSG00000155330 20784   -0.0029102815315127373  -0.938641   Yes
row_78  ENSG00000069849 20797   -0.002954457188025117   -0.9375976  Yes
row_79  ENSG00000137331 20808   -0.0030260297935456038  -0.9364197  Yes
row_80  ENSG00000115461 20830   -0.0030999367590993643  -0.9357257  Yes
row_81  ENSG00000138772 20850   -0.00321852695196867    -0.9348715  Yes
row_82  ENSG00000100906 20853   -0.003225792432203889   -0.9332032  Yes
row_83  ENSG00000168014 20860   -0.0032534662168473005  -0.9317104  Yes
row_84  ENSG00000169429 20872   -0.0033344782423228025  -0.9304116  Yes
row_85  ENSG00000066468 20896   -0.0034734471701085567  -0.92960864 Yes
row_86  ENSG00000241878 20923   -0.003696492640301585   -0.9288267  Yes
row_87  ENSG00000020577 20951   -0.004143731202930212   -0.92784786 Yes
row_88  ENSG00000072110 20963   -0.004316456150263548   -0.92601216 Yes
row_89  ENSG00000189060 20977   -0.0045832977630198     -0.9241259  Yes
row_90  ENSG00000091137 20980   -0.004666114691644907   -0.9216702  Yes
row_91  ENSG00000198959 20998   -0.005465275142341852   -0.91949236 Yes
row_92  ENSG00000168386 21003   -0.0055362265557050705  -0.91665626 Yes
row_93  ENSG00000143799 21008   -0.005979615729302168   -0.91357774 Yes
row_94  ENSG00000133639 21022   -0.10809064656496048    -0.85510254 Yes
row_95  ENSG00000227105 21056   -0.24622434377670288    -0.7220608  Yes
row_96  ENSG00000101335 21065   -0.28613466024398804    -0.5660082  Yes
row_97  ENSG00000105655 21070   -0.29664647579193115    -0.404018   Yes
row_98  ENSG00000120129 21075   -0.33157122135162354    -0.22293401 Yes
row_99  ENSG00000111328 21077   -0.40846818685531616    3.336285E-4 Yes
```
</details>

Como se puede observar, los resultados de ambos análisis son muy similares.
